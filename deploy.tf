locals {
  tags = []
}

module "group" {
  source = "./modules/group"

  for_each = local.groups

  name                    = each.key
  path                    = lookup(each.value, "path")
  description             = lookup(each.value, "description")
  parent_id               = lookup(each.value, "parent_id", 0)
  group_memberships       = lookup(each.value, "group_memberships", {})
  project_creation_level  = lookup(each.value, "project_creation_level", "maintainer")
  subgroup_creation_level = lookup(each.value, "subgroup_creation_level", "owner")
  request_access_enabled  = lookup(each.value, "request_access_enabled", false)
}

module "project" {
  source = "./modules/project"

  for_each = merge(
    local.terraform_projects,
    local.docker_projects,
    local.pipelines_projects,
    local.templates_projects
  )

  group_id = lookup(each.value, "group_id")

  name                             = each.key
  description                      = lookup(each.value, "description")
  import_url                       = lookup(each.value, "import_url", null)
  tags                             = lookup(each.value, "tags", local.tags)
  default_branch_push_access_level = lookup(each.value, "default_branch_push_access_level", "no one")
  container_registry_enabled       = lookup(each.value, "container_registry_enabled", false)
  pages_access_level               = lookup(each.value, "pages_access_level", "disabled")
}

locals {
  templates_projects = {
    terraform-module : {
      description = "Template to initiate a new terraform module."
      tags        = concat(local.terraform_module_tags, ["template", "terraform", "module"])
      group_id    = module.group["templates"].id
    }
    terraform-deployment : {
      description = "Template to initiate a new terraform deployment."
      tags        = concat(local.terraform_module_tags, ["template", "terraform", "module"])
      group_id    = module.group["templates"].id
    }
    docker : {
      description = "Template to initiate a new docker image."
      tags        = concat(local.terraform_module_tags, ["template", "docker"])
      group_id    = module.group["templates"].id
    }
  }
}

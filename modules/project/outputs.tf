output "projects" {
  value     = gitlab_project.this
  sensitive = true
}

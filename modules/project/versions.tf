terraform {
  required_version = ">= 0.14.9"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.6"
    }
  }
}

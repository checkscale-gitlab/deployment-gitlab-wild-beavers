terraform {
  required_version = ">= 0.15.1"
  experiments      = [module_variable_optional_attrs]

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.6"
    }
  }
}

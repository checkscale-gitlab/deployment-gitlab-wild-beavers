output "id" {
  value = gitlab_group.this.id
}

output "full_path" {
  value = gitlab_group.this.full_path
}

output "full_name" {
  value = gitlab_group.this.full_name
}

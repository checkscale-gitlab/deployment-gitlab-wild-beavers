#####
# Projects
#####

output "projects" {
  value = { for key, project in module.project : key => {
    id : project.projects.id,
    path_with_namespace : project.projects.path_with_namespace,
    ssh_url_to_repo : project.projects.ssh_url_to_repo,
    http_url_to_repo : project.projects.http_url_to_repo,
    web_url : project.projects.web_url,
    runners_token : project.projects.runners_token,
    }
  }
  sensitive = true
}

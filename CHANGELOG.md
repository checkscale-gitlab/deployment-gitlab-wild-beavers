0.3.0
=====

* feat: (Terraform) add repository `module-nexus-user`
* chore: use `https` instead of `git` protocol for pre-commit hooks
* chore: bump pre-commit hooks

0.2.0
=====

* feat: add docker jenkins master repo

0.1.0
=====

* feat: add new repos concerning ECS

0.0.1
=====

* feat: add variable to enable or not container registry on repo

0.0.0
=====

* tech: initialise repository
